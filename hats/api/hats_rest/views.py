from django.http import JsonResponse
import json
from .models import LocationVO, Hat
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ['import_href', 'closet_name']


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric','style', 'color', 'picture_url', 'location']

    encoders = {
        "location": LocationVODetailEncoder(),
    }
    
    def get_extra_data(self, o):
        return {'id': o.id}


class HatEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric', 'style', 'color', 'picture_url', 'location']

    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def api_list_hats(request, location_vo_id=None):
    if request.method == 'GET':
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
            return JsonResponse(
                {'hats': hats},
                encoder=HatListEncoder,
            )
        else:
            hats = Hat.objects.all()
            return JsonResponse(
                {'hats': hats},
                encoder=HatListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        return JsonResponse(
            {'message': 'You havent set up PUT method yet.'},
        )
