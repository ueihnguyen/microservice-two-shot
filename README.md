# Wardrobify

Team:

* Mitchell Hom - Shoes Microservice
* Tony Nguyen - Hat Microservice

## Design
![alt text](assets/practiceWadrobeAPI.png)


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

![alt text](assets/shoes-diagram.png)


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

![alt text](assets/full-hats-model.png)
