import React, { useEffect, useState } from "react";

function HatForm() {

  const [locations, setLocations] = useState([]);
  const [formInput, setFormInput] = useState({
    fabric: '',
    style: '',
    color: '',
    picture_url: '',
    location: '',
  });

  useEffect(() => {
    fetch("http://localhost:8100/api/locations/")
      .then(response => response.json())
      .then(data => setLocations(data.locations))
  },[]);

  const handleInputChange = (e) => {
    setFormInput({
      ...formInput,
      [e.target.id]: e.target.value
    })
  }

  const clearState = () => {
    setFormInput({
        fabric: '',
        style: '',
        color: '',
        picture_url: '',
        location: '',
    })
  }

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = {...formInput};
    const response = await fetch('http://localhost:8090/api/hats/', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      clearState();
    }
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleFormSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={formInput.fabric} onChange={handleInputChange} placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formInput.style} onChange={handleInputChange} placeholder="Style" required type="text" id="style" name="style" className="form-control"/>
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formInput.color} onChange={handleInputChange} placeholder="Color" type="text" id="color" name="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formInput.picture_url} onChange={handleInputChange} placeholder="Picture URL" required type="text" id="picture_url" name="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={formInput.location} onChange={handleInputChange} required id="location" className="form-select" name="location">
                  <option>Choose a location</option>
                  {locations.map( location => <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
