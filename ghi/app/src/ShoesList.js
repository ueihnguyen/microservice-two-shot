import React from 'react';
import { Link, NavLink } from 'react-router-dom';
// import { Navigate } from 'react-router';

function ShoeColumn(props) {
    return (
      <div className="col">
        {props.list.map(data => {
          const shoe = data;
          return (
            <div key={shoe.href} className="card mb-3 shadow">
                <NavLink to={`/shoes/detail/${shoe.id}/`}>
                    <img src={shoe.picture} alt="..." className="card-img-top" />
                </NavLink>
                <div className="card-body">
                    <h5 className="card-title">{shoe.name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                    {shoe.manufacturer}
                    </h6>
                </div>
            </div>
          );
        })}
      </div>
    );
  }

  class MainShoePage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        shoeColumns: [[], [], []],
      };
    }

    async componentDidMount() {
      const url = 'http://localhost:8080/api/bins/shoes/';

      try {
        const response = await fetch(url);
        if (response.ok) {
          // Get the list of shoes
          const data = await response.json();
          console.log(data)

          // Create a list of for all the requests and
          // add all of the requests to it
          const requests = [];
          for (let shoe of data.shoes) {
            const detailUrl = `http://localhost:8080${shoe.href}`;
            requests.push(fetch(detailUrl));
          }

          // Wait for all of the requests to finish
          // simultaneously
          const responses = await Promise.all(requests);
          // console.log("response --- ", responses)
          // Set up the "columns" to put the shoe
          // information into
          const shoeColumns = [[], [], []];

          // Loop over the shoe detail responses and add
          // each to to the proper "column" if the response is
          // ok
          let i = 0;
          for (const shoeResponse of responses) {
            if (shoeResponse.ok) {
              const details = await shoeResponse.json();
              shoeColumns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(shoeResponse);
            }
          }
          // console.log(shoeColumns)

          // Set the state to the new list of three lists of
          // conferences
          this.setState({shoeColumns: shoeColumns});
        }
      } catch (e) {
        console.error(e);
      }
    }

    render() {
      return (
        <>
          <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
            <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
            <h1 className="display-5 fw-bold">Shoe App</h1>
            <div className="col-lg-6 mx-auto">
              <p className="lead mb-4">
                The best shoes in the world
              </p>
              <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Create a shoe</Link>
              </div>
            </div>
          </div>
          <div className="container">
            <h2>Our shoe collection</h2>
            <div className="row">
              {this.state.shoeColumns.map((ShoeList, index) => {
                return (
                  <ShoeColumn key={index} list={ShoeList} />
                );
              })}
            </div>
          </div>
        </>
      );
    }
  }

  export default MainShoePage;
