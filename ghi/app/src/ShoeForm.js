import React, { useState, useEffect } from 'react';

function ShoeForm(props){
    const [ name, setName ] = useState('');
    const [ manufacturer, setManufacturer ] = useState('');
    const [ color, setColor ] = useState('');
    const [ picture, setPicture ] = useState('');
    const [ Bin, setBin ] = useState('');
    const [ bins, binFetch ] = useState([]);

    const handleSubmit = async (sumbit) => {
        sumbit.preventDefault()
        //note* if you want to have consistent naming convetion we would use camelCasing
        // and pass that into our body "{max_attendees: maxAttendee}"
        const data  = {name, manufacturer, color, picture}
        const ShoesUrl = `http://localhost:8080/api/bins/${Bin}/shoes/`;
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(ShoesUrl, fetchConfig);
        if (response.ok){
            const newShoe = await response.json();
            clearState()

        }
    }
    const clearState = () => {
        setBin("")
        setPicture("")
        setColor("")
        setManufacturer("")
        setName("")
    }

    useEffect(() => {
        const fetchBins = async () => {
            //get the bins data to map to later
            const url = 'http://localhost:8100/api/bins/'
            const response = await fetch(url)
            const data = await response.json()
            binFetch(data.bins)
            // console.log('data', data.bins)
        }

        fetchBins()
    }, [])

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form id="create-shoe-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="name" value={name} onChange={(event) => setName(event.target.value)} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="picture" value={picture} onChange={(event) => setPicture(event.target.value)} required type="text" name="picture" id="picture" className="form-control"/>
                <label htmlFor="picture">Picture</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Manufacturer" value={manufacturer} onChange={(event) => setManufacturer(event.target.value)} required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Color" value={color} onChange={(event) => setColor(event.target.value)} required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select required id="Bin" value={Bin} onChange={(event) => setBin(event.target.value)} name="location" className="form-select">
                    <option>Choose a Bin</option>
                  {bins.map(bin => (
                  <option key={bin.id} value={bin.id}>{bin.closet_name}</option>))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>

    )
}

export default(ShoeForm)
