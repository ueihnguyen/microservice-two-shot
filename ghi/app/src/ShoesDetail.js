import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router';
import { useParams } from 'react-router-dom';

function ShoeDetail(props){
    const [data, dataFetch] = useState([])
    const [bin, binData ] = useState({})
    const { id } = useParams();


    const DeleteConfirmation = () => {
        const card = document.getElementById('card');
        const DeleteConfirmation = document.getElementById('alert')
        card.classList.add("d-none")
        DeleteConfirmation.classList.remove("d-none")
    }
    const NotDeleting = () => {
        const card = document.getElementById('card');
        const DeleteConfirmation = document.getElementById('alert')
        card.classList.remove("d-none")
        DeleteConfirmation.classList.add("d-none")
    }

    const ConfirmDeletion = async (e) => {
        e.preventDefault()
        const shoeUrl = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const sendToShoeList = document.getElementById('sendBack');
            const DeleteConfirmationPage = document.getElementById('alert')
            DeleteConfirmationPage.classList.add("d-none")
            sendToShoeList.classList.remove("d-none")


        }else{
            console.error(response)
        }
    }

    const navigate = useNavigate();


    useEffect(() => {
        const fetchData = async () => {
            //get all our data from our details
            const url = `http://localhost:8080/api/shoes/${id}/`
            const response = await fetch(url)
            const data = await response.json()
            console.log(data)
            dataFetch(data)
            binData(data.Bin)
        }

        fetchData()
    }, [])
    const closet_name = bin.closet_name



    return (
    <div className="container">
        <div className="card" id="card">
                <img src={data.picture} className="card-img-top" alt="..."/>
            <div className="card-body">
                <h5 className="card-title">{data.name}</h5>
            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget ornare ante. Nunc et lectus vulputate, elementum magna in, gravida est. In bibendum efficitur congue</p>
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item">{data.manufacturer}</li>
                <li className="list-group-item">{data.color}</li>
                <li className="list-group-item">This can be found in our {closet_name} closet</li>

            </ul>
            <div className="card-body">
                <button href="#" onClick={DeleteConfirmation} className="card-link">Delete</button>
            </div>
        </div>
        <div className="alert alert-success d-none" role="alert" id="alert" style={{backgroundColor:"#DBF7DE"}}>
            <h4 className="alert-heading">Confirm Delete</h4>
            <p>Are you sure you want to delete this shoe?</p>
            <hr />
            <div className="d-grid gap-2 d-md-block" >
                <button type="button" onClick={ConfirmDeletion} className="btn btn-success m-2">Yes</button>
                <button type="button" onClick={NotDeleting} className="btn btn-danger">No</button>
            </div>
        </div>
        <button type="button" className="btn btn-lg btn-danger d-none" onClick={() => navigate("/shoes")} id="sendBack" data-bs-toggle="popover" data-bs-title="Popover title" data-bs-content="">Click here to go back to shoes list</button>

    </div>

    )
    }
export default ShoeDetail
