import React, { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function HatDelete() {
  const params = useParams();

  const [deleteCheck, setDeleteCheck] = useState(false);
  const [hatDetail, setHatDetail] = useState({});

  useEffect(() => {
    fetch(`http://localhost:8090/api/hats/${params.hatId}`)
      .then((response) => response.json())
      .then((data) => setHatDetail(data));
  }, []);

  const handleDelete = async (e) => {
    e.preventDefault();
    const response = await fetch(
      `http://localhost:8090/api/hats/${params.hatId}`,
      {
        method: "DELETE",
        body: {},
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.ok) {
      const msg = await response.json();
      console.log(msg);
      setDeleteCheck(true);
    }
  };

  if (deleteCheck) {
    return <Navigate to="/hats" />;
  } else {
    return (
      <>
        <div className="alert alert-danger m-5 p-5" role="alert">
          <h3>Are you sure you want to delete this Hat?</h3>
          <h5>
            a {hatDetail.color} Hat, made of {hatDetail.fabric}, in{" "}
            {hatDetail.style} style
          </h5>
          <div className="text-center">
            <Link
              className="btn btn-success btn-lg me-4"
              to={`/hats/${params.hatId}`}
              role="button"
            >
              No, bring me back
            </Link>
            <button
              onClick={handleDelete}
              type="button"
              className="btn btn-danger btn-lg me-4"
            >
              Yes, delete it
            </button>
          </div>
        </div>
      </>
    );
  }
}

export default HatDelete;
