import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsList from './HatsList';
import MainPage from './MainPage';
import HatDetail from './HatDetail';
import HatForm from './HatForm';
import HatDelete from './HatDelete';
import Nav from './Nav';

function App(props) {

  if (props.hats === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="hats" >
          <Route index element={<HatsList hats={props.hats}/>} />
          <Route path="new" element={<HatForm />} />
          <Route path=':hatId'>
            <Route index element={<HatDetail />} />
            <Route path='delete' element={<HatDelete />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
