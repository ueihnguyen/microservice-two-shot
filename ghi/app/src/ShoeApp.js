import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDetail from './ShoesDetail';

function App(props) {
  if (props.shoes === undefined) {
    return "ok"
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />}/>
            <Route path="new" element={<ShoeForm />} />
            <Route path="detail/:id" element={<ShoeDetail />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
