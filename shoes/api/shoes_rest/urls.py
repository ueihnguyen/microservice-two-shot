from django.urls import path
from .views import api_list_shoes, api_show_shoes

urlpatterns = [
    path("bins/shoes/", api_list_shoes, name="api_create_shoe"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoe",
    ),
    path("shoes/<int:pk>/", api_show_shoes, name="api_show_shoes"),
]
