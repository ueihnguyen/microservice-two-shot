from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
# Create your views here.

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "picture","color", "manufacturer", "id"]

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "color", "picture", "manufacturer", "Bin", "id"]

    encoders = {
        "Bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id != None:
            shoes = Shoe.objects.filter(Bin=bin_vo_id)
            return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder,)
        else:
            shoes = Shoe.objects.all()
            return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder,)

    else:
        content = json.loads(request.body)
        
        try:
            # does the bin even exist?
            bin_href = f'/api/bins/{bin_vo_id}/'
            print("HREF____", bin_href)
            bin_id = BinVO.objects.get(import_href=bin_href)
            print("BinID____", bin_id)
            content["Bin"] = bin_id

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        new_shoe = Shoe.objects.create(**content)
        return JsonResponse(
            new_shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoes, encoder=ShoeDetailEncoder, safe=False
        )
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
